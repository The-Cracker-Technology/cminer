cd src

g++ -o Cminer Cminer.cpp -s

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "G++ Compile... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf Cminer /opt/ANDRAX/bin

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Binary copy... PASS!"
else
  # houston we have a problem
  exit 1
fi

chmod -R 755 /opt/ANDRAX/bin
chown -R andrax:andrax /opt/ANDRAX/bin
